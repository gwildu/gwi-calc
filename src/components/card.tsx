import { ICalculation } from "../data/calculation.type.ts";
import { FunctionComponent } from "react";

export const Card: FunctionComponent<{
  card: ICalculation;
  showResult: boolean;
}> = ({ card, showResult }) => {
  return (
    <div key={card.displayQuest}>
      <p>{card.displayQuest}</p>
      <p>{showResult ? card.result : "?"}</p>
    </div>
  );
};
