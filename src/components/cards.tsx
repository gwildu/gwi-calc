import { FunctionComponent, useRef, useState } from "react";
import { allCalculations } from "../data/calculation.ts";
import { Card } from "./card.tsx";
import { shuffle } from "../data/calculation.helpers.ts";

export const Cards: FunctionComponent = () => {
  const [count, setCount] = useState(0);
  const [calculations, setCalculations] = useState(shuffle(allCalculations));

  const currentCalculation = calculations[count];
  const increment = () => {
    setCount(count < calculations.length ? count + 1 : 0);
    setCalculations(shuffle(allCalculations));
  };

  const inputRef = useRef<HTMLInputElement>(null);
  const [inputResult, setInputResult] = useState<string>("");
  console.log({ inputResult });
  const [showResult, setShowResult] = useState<boolean>(false);

  console.log("shuffledCalculations", calculations);

  const onChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const newResult = e.target.value;
    setInputResult(newResult);
    if (newResult === currentCalculation.result.toString()) {
      setShowResult(true);
    }
  };

  const reset = () => {
    setInputResult("");
    setShowResult(false);
    inputRef.current?.focus();
  };

  const onNext = () => {
    reset();
    increment();
  };

  return (
    <>
      <Card card={currentCalculation} showResult={showResult} />
      <div>
        <input
          ref={inputRef}
          type={"text"}
          value={inputResult ?? ""}
          onChange={onChange}
          onKeyDown={(e) => {
            e.key === "Enter" && showResult && onNext();
          }}
        />
      </div>
      <div>
        <button
          disabled={!showResult}
          onClick={onNext}
          style={{ marginTop: "20px" }}
        >
          next
        </button>
      </div>
    </>
  );
};
