import { ICalculation } from "./calculation.type.ts";

const zero2nintynine = [...Array(100).keys()];
export const allAdditions = zero2nintynine.reduce((previous, argument1) => {
  const calculations: ICalculation[] = zero2nintynine
    .filter((argument2) => argument2 !== argument1)
    .map((argument2) => ({
      type: "addition",
      displayQuest: `${argument1} + ${argument2}`,
      result: argument1 + argument2,
      argument1,
      argument2,
    }));
  const filtered = calculations.filter(
    (calculation) => calculation.result <= 100,
  );
  return [...previous, ...filtered];
}, [] as ICalculation[]);

export const allSubstractions: ICalculation[] = allAdditions.map(
  ({ argument1, argument2, result }: ICalculation) => ({
    type: "substraction",
    displayQuest: `${result} - ${argument1}`,
    result: argument2,
    argument1: result,
    argument2: argument1,
  }),
);
