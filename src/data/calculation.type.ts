export interface ICalculation {
  type: "addition" | "substraction" | "multiplication" | "division";
  argument1: number;
  argument2: number;
  displayQuest: string;
  result: number;
}
