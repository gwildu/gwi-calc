import { allAdditions, allSubstractions } from "./additionAndSubstraction.ts";
import {
  allDivisions,
  allMultiplications,
} from "./multiplicationAndDivision.ts";
import { ICalculation } from "./calculation.type.ts";

export const allCalculations: ICalculation[] = [
  ...allAdditions,
  ...allSubstractions,
  ...allMultiplications,
  ...allDivisions,
];
