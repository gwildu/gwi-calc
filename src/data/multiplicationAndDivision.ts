import { ICalculation } from "./calculation.type.ts";

const zero2twelve = [...Array(13).keys()];

export const allMultiplications: ICalculation[] = zero2twelve.reduce(
  (previous, argument1) => {
    const calculations: ICalculation[] = zero2twelve
      .filter((argument2) => argument2 !== argument1)
      .map((argument2) => ({
        type: "multiplication",
        displayQuest: `${argument1} x ${argument2}`,
        result: argument1 * argument2,
        argument1,
        argument2,
      }));
    return [...previous, ...calculations];
  },
  [] as ICalculation[],
);

export const allDivisions: ICalculation[] = allMultiplications.map(
  ({ argument1, argument2, result }: ICalculation) => ({
    type: "division",
    displayQuest: `${result} : ${argument1}`,
    result: argument2,
    argument1: result,
    argument2: argument1,
  }),
);
